# Interoperability opportunities
One of our goals is to collaborate with other ESSIF-lab community members to improve our solution. Another reason to work with the ESSIF-lab community is that we are passionate about technology and innovation, and we want to work with as many partners as possible to help SSI move forward. We currently see various concerns with current authorization methods (low security, multiple passwords, verification before issuance of credentials, and etc.), and we are happy to contribute to SSI's elimination of these issues. <br/>
<br/>
To ensure the success of our solution, we must master the following processes: identity verification before issuance of credentials; issuing credentials; holding credentials (wallet); credential verification; and integration. We believe that in order to be successful, we must work with other ESSIF-lab community members, and after examining all of the projects, we have produced a list of possible collaborators. We describe in detail how we foresee interoperability with other eSSIF-lab members below. <br/>

![Interop opportunities](Images/Envisioned_interop_opportunities.png)

# Envisioned interoperability
| Company | Identity verification | Issuing credentials | Holding credentials  | Credential verification | Integration |
| ------- |:---------------------:| :------------------:| :-------------------:| :----------------------:| :----------:|
| Quadible  | + |  |  |
| Visma |    |  +  |
| BCdiploma |   |  + | + | + |
| yes.com  | + |  |  |
| NETIS |    |  +  |   |  | + |
| Commerc.io |  +  | + |  |
| e-Origin  |   |   | + |+
| UBITECH |    |    |  +  |
| GIMLY |    |   | + |
| GATACA |    |  + | + |+|+
| Joinyourbit |    | +  | |
| PolicyMan |    | +  | | |+
| Danube Tech |    |   | + | |

![Interop report](https://gitlab.grnet.gr/essif-lab/cfoc/sisuab/deliverables/-/raw/master/Images/Envisioned_interop_report.jpg)
Having created open API's and universal solution CorpoSign DID, we believe that we can truly make DIDs and SSI truly unified across other ESSIF-lab community members. CorpoSign DID is fully compliant with EBSI, ESSIF, DTLF and W3C interoperability standards for decentralized identifiers and verifiable credentials.  

### Identity verification
**Quadible** - OnboardSSI will employ AI to remotely validate users' identities and create verified credentials without the need for human validation. One of the challenges we encountered when designing our prototype was the requirement to approve a person before granting credentials. If we could locate someone who could supply it, we feel that identity verification would be the most useful to us. We contacted Quadible, but they are still working on their solution. However, we agreed that once their product is more developed, we will try to integrate it with ours. <br/>
<br/>
**Yes.com** - It is a company that specializes in getting and presenting diplomas with bank-issued credentials. We may apply a similar method when issuing our credentials.<br/>
<br/>
**Commerc.io** - includes a protocol that allows for the rapid generation of a KYC VC (Verifiable Credential) based on data accessible via PSD2 Bank Payment Service Directives. Interoperability with them would allow us to swiftly authenticate a person's identity and allow them to access our services by granting them VC, which would most likely be their primary Identity Credential because to its high dependability.<br/>
<br/>

### Issuing credentials
**Visma** - It is establishing an SSI Mandate service. It will make it possible to issue verifiable mandates, allowing their delegates to act in their place. We met with Visma and agreed to share critical information because our solutions aim to tackle comparable problems. <br/>
<br/>
**BCdiploma** - has a method for issuing and retaining diplomas as verified credentials. Even if our solution does not aim to award degrees, we may trade ideas or even try to incorporate one other's wallets into one another. <br/>
<br/>
**Netis** - Because Netis focuses on addressing similar issues, their work on Verifiable Mandates might be relevant in choosing the components of credential schema. <br/>
<br/>
**Commerc.io** - Commerc.io can provide credentials with a high degree of certainty since they employ banks to verify user information, therefore their supplied VC would be trustworthy and could be stored inside our app solution and used for SSI login to other websites, as stated in the Identity verification part.<br/>
<br/>
**GATACA** - is developing tools for universities and other enterprises to issue VC, with the possibility of holding their issued VC within our wallet and providing them with our own. Because both systems offer the possibility of utilizing the EBSI ecosystem, it would be reasonable to attempt to combine both wallets in order to increase interoperability.<br/>
<br/>
**Joinyourbit** - are intending to integrate SSI credentials that may be stored in our wallet and used for SSI authentication; the only barrier is that they intend to utilize eIDAS eSeal, which we do not yet have, but if done, it will considerably improve security and VC assurance level. <br/>
<br/>
**PolicyMan** - They enable access to various resources by supplying various VC. Our solution could save those VC and display them when needed.<br/>

### Holding credentials
**BCdiploma** - Because BCdiploma is developing its own SSI system, we may be able to exchange verifiable credentials, as we have also built the ability to issue, hold, and verify e-student ID.<br/>
<br/>
**e-Origin** - We might attempt to keep our credentials in their wallet. Also, it would be a good idea to cooperate on customs and e-CMR interoperability. <br/>
<br/>
**UBITECH** - By adopting UBITECH's DOOR component, we could make our credentials more safe by linking Verifiable Credentials (VCs) to the wallet.<br/>
<br/>
**Gimly** - Their SSI-NFC bridge might allow the establishment of NFC-based SSI credentials for drivers, government officials, and other logistics operation participants.<br/>
<br/>
**GATACA** - incorporates the EBSI ecosystem, so keeping our credentials would be no problem and would provide an opportunity to become more closely tied solutions.<br/>
<br/>
**Danube Tech** - As we both support EBSI and are creating universal wallets, they may try to integrate with us to keep our VC.<br/>

### Credential verification
**BCdiploma** - We should be able to validate each other's student IDs, diplomas, or certificates after the above-mentioned interoperability development with BCdiploma.<br/>
<br/>
**GATACA** - We could arrange it such way, that we could validate their granted VC.<br/>

### Integration
**Netis** - SSI-as-a- Netis' service facilitates SSI integration and adoption by assisting businesses in selecting an SSI solution provider. We could contact more potential clients that wanted to deploy our authorisation solution with the aid of netis. <br/>
<br/>
**GATACA** - The greatest possibility is probably being able to utilize one another's SSI solutions to authenticate to employing VC that both could check its legitimacy. This would make wallets interchangeable for SSI access.<br/>
<br/>
**e-Origin** - A good opportunity to connect and integrate their customs declarations with our e-CMR and vice versa.<br/>
<br/>
**PolicyMan** - Their major goal is to provide different levels of access to SSI by issuing different VC. This allows us to fine-tune access to various resources and is critical for integrating our SSI solution with theirs.<br/>


