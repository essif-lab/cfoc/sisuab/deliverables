<b>CorpoSign Decentralised Identity (SSI) demo video:
https://www.youtube.com/watch?v=fIo_Gdz80K8

<b>Description:

CorpoSign DID is a decentralised identity framework that uses blockchain and cryptographic technology to easily identify and connect people with government, businesses, data, and services. It is fully compliant with EBSI and W3C interoperability standards for decentralised identifiers and verifiable credentials.

- Issue, verify and control your cross-border credentials. A verifiable credential is like an e-passport, but for any trade or legal document.  

- Empower your community members simply by issuing Verifiable credentials to complement existing, paper/pdf processes, starting immediately. 

- Eliminate counterfeits by employing a chain of verifiable credentials (IP authority, customs list of duty paid packs).

- Trade documents that are scalable, rapid, and trustworthy to boost cross-border compliance and cut waiting times.

Users are regaining ownership of their data and identities thanks to CorpoSign DID's decentralised, self-sovereign, privacy-preserving, and user-friendly nature.

<b>Solution background:
_______________________
SSI based authorisation for cross-border government and business representatives by Systems Integration Solutions, UAB.

Currently there are no authorisation mechanisms that could link natural persons to organisation in the cross-border logistics ecosystem. Problems occur if the organisation and the representative are from different countries or the representative does not agree to use their personal identifier for organisational purposes. As well, lack of such a solution results in inability to ensure interoperability, security, and to prevent fraud. CorpoSign DID will allow to authorise the representative (by issuing him verifiable credentials). It will ensure better governance, security, interoperability, will become an enabler to speed up the eCMR development/implementation process.
