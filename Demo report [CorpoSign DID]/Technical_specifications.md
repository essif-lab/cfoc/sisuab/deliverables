CorpoSign DID (ESSIF4Logistics) technical specifications
========================
## Project Schema V1    

![Concept](Images/ESSIF4Logistics_schema.png)

The graph above depicts a broad understanding of ESSIF4Logistics. It is based on the interactions of the Issuer, Holder, and Verifier. The infrastructure of this procedure is shown below.

## Project Schema V2    

![Concept](https://gitlab.grnet.gr/essif-lab/cfoc/sisuab/deliverables/-/raw/master/Images/eSSIF4Logistics_schema_v2.png)

The image above depicts an updated notion of ESSIF4Logistics in accordance with the EBSI Self-Issued OpenID Provider V2 standard.
The primary differences are that Verifier can now be a unified solution (anyone), in which case just Issuers and Holders are required to possess DIDs. 

Process modification: 
Verifiers - Verifies the validity of the issuer and the revocation of VCs and schemas. Issuers -  Use Schemas, register public information, DIDs, and cancel VCs; 
Holders: Do not keep DID papers (JWK) in the ledger; instead, keep them in their wallets. Holder DIDs (Subject Identifier bytes) are now JWK Thumbprint from Public Key (sha-256, 32 bytes), instead of random 16 bytes.

Overall, the aforementioned adjustments result in even stronger decentralization than before.

## Technical infrastructure

![Concept](Images/ESSIF4logistics_infrastructure.png)

To receive credentials, a user must first enroll on the issuer's website, and then the credentials are issued using the Issuer's API. The credentials are then presented as a QR code. To transfer these credentials to our company's wallet (CorpoSign DID app), a user must use a wallet to scan this QR code. As seen above, our system makes extensive use of EBSI APIs. EBSI is also a trustworthy registry. If a user wishes to verify the credentials' legitimacy, he or she must go to a verifier's website, scan the QR code with a wallet, then choose the credentials, and the verifier webpage will provide the relevant data using Verifier's API. It is also possible to use Verifiable Credential to go through KYC simultaneously login in to the system or application. APIs, the issuer's website, the verifier's website, and the credentials schemas are all provided below.

## Servers infrastructure 

![Concept](Images/ESSIF4Logistics_servers_infrastructure.png)

Modules A, B, C in this schema above, are the abstract components as shown in the "Components Technical Infrastructure" schema above. For several years, our business Systems Integration Solution has been creating a best practice for services architecture based on micro-services. It is a higher-level architecture for developing service-based applications, with the service serving as the smallest and most functional unit of work in the application. The micro-services architecture enables us to create services that are created in a very compact and specialized manner, allowing them to execute highly exact and timely functions. 

Our solution might be executed on a Docker virtual machine or a Kubernetes cluster. We use Kubernetes to automate containerized application deployment and administration. Kubernetes' actual strength derives from its nearly infinite scalability, configurability, and extensive technological ecosystem, which includes various open-source frameworks for monitoring, administration, and security.

It was designed in such a way that services may be disconnected from the rest of the application and run completely independently of it. Please contact info@sis.lt for further decoupling of server / deployment infrastructure or if you require implementation of our products on your chosen infrastructure.

## Components

| Component | Description | Contribution | Source |
| --------- |-------------| -------------| -------------|
| Holder app <br/> Wallet (CorpoSign DID app) |Can create and register DIDs in EBSI <br/> ecosystem. Can hold verifiable credentials, <br/> provide verifiable presentations. | Allows user to hold and present power of attorney, natural person, verfiable attestation - diploma <br/> credentials.| https://play.google.com/store/apps/details?id=app.sis.mydid.net|
| Issuer API | EBSI, W3C and OpenID conforming API for issuing <br/> verifiable credentials. | Allows businesses and goverment organizations to issue power of attorney <br/> verifiable credentials to enrolled employees and goverment officials.| https://api-v2.did.sis.lt/docs|
| Issuer Webapp | Allows organisation users to authenticate and <br/> receive credentials. | Allows users to receive power of attorney <br/> credentials.| https://issuer1.essif.sis.lt/|
| Verifier API | EBSI, W3C and OpenID conforming API for verifying <br/> verifiable credentials and verifiable <br/> presentations. | Allows organisations to verify issued verifiable <br/> credentials and verifiable presentations. | https://api.verifier.essif.sis.lt/docs|
| Verifier Webapp |Allows users to authenticate with credentials.| Allows users to authenticate and access allowed <br/> resources of the represented organisation. | https://verifier.essif.sis.lt/?verifier=vc-790e50f2&ip=true&geo=true&timeout=500|
| EBSI API <br/> infrastructure | It is used for DID legal entity document registration. | Allows verifying DID and trusted issuers. | https://api.preprod.ebsi.eu/docs|
| Alternative API Schemas | Our own created credential schema for Business and Goverment Power of attorney. | Allows business and government officials to represent their organizations. |https://essif-schemas-4yn7xr7v2kopdv.s3.eu-west-1.amazonaws.com/ESSIF+Power+of+Attorney+(Business).json <br/> https://essif-schemas-4yn7xr7v2kopdv.s3.eu-west-1.amazonaws.com/ESSIF+Power+of+Attorney+(Government).json |
